package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    public final static String EXIT = "Q";
    public Logger logger = LogManager.getLogger(View.class);
    private Controller controller = new Controller();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show serialization and deserialization");
        menu.put("2", "\t2 - Show usual reading");
        menu.put("3", "\t3 - Show usual buffered reading");
        menu.put("4", "\t4 - Show directory content");
        menu.put("5", "\t5 - Show result for read and write data from/to chanel");
        menu.put("6", "\t6 - Show usual buffered reading to size");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showSerialization);
        methodsMenu.put("2", this::showUsualFileReading);
        methodsMenu.put("3", this::showBufferedFileReading);
        methodsMenu.put("4", this::showDirectoryContent);
        methodsMenu.put("5", this::showResultForReadWriteData);
//        methodsMenu.put("6", this::showBufferedReadingToSize);
    }

    private void showSerialization() {
        controller.getSerialization();
        controller.getDeserialization();
    }

    private void showUsualFileReading() throws IOException {
        controller.getUsualReading();
    }

    private void showBufferedFileReading() throws IOException {
        controller.getBufferedReading();
    }

    private void showDirectoryContent() throws IOException {
        controller.getDirectory();
    }

    private void showResultForReadWriteData() {
        controller.readWriteData();
    }

    private void outputMenu() {
        logger.warn("\nMENU:");
        for (String str : menu.values()) {
            logger.warn(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.warn("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (!keyMenu.equals(EXIT));
    }
}
