package com.epam.controller;

import com.epam.model.readfile.CompareReading;
import com.epam.model.serialization.BookSerialization;
import com.epam.model.showcontent.Content;
import com.epam.model.somebuffer.SomeBuffer;

import java.io.IOException;

public class Controller {
    BookSerialization bookSerialization = new BookSerialization();
    CompareReading compareReading = new CompareReading();
    Content content = new Content();
    SomeBuffer someBuffer = new SomeBuffer();

    public void getSerialization() {
        bookSerialization.serialize();
    }

    public void getDeserialization() {
        bookSerialization.deserialize();
    }

    public void getUsualReading() throws IOException {
        compareReading.getUsualReader();
    }

    public void getBufferedReading() throws IOException {
        compareReading.getBufferedReader();
    }

    public void getDirectory() throws IOException {
        content.getContent();
    }

    public void readWriteData(){
        someBuffer.readWriteFile();
    }

}
