package com.epam;

import com.epam.view.View;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        new View().show();

    }
}
