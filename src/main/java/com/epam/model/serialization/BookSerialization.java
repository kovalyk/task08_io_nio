package com.epam.model.serialization;

import java.io.*;

public class BookSerialization {
    String fileName = "/home/traveler1357/Epam/Lesson12/src/main/resources/save.txt";
    Book book = new Book("Fahrenheit 451", "Ray Bradbury", "Classic", 2011);

    public void serialize() {
        try (FileOutputStream file = new FileOutputStream(fileName);
             ObjectOutputStream outputStream = new ObjectOutputStream(file)) {
            outputStream.writeObject(book);
            System.out.println("Object is serialized");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void deserialize() {
        try (FileInputStream file = new FileInputStream(fileName);
             ObjectInputStream inputStream = new ObjectInputStream(file)) {
            Book book = (Book) inputStream.readObject();
            System.out.println("Object is deserialized ");
            System.out.println("book = " + book.toString());
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("IOException is caught");
        }
    }
}
