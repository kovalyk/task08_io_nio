package com.epam.model.somebuffer;


import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SomeBuffer {
    public void readWriteFile() {
        String filePath = "/home/traveler1357/Epam/Lesson12/src/main/resources/Task7.txt";
        try {
            Path path = Paths.get(filePath);
            FileChannel channel = FileChannel.open(path);
            ByteBuffer buffer = ByteBuffer.allocate(20);
            int i = channel.read(buffer);
            while (i != -1) {
                System.out.println(i);
                buffer.flip();
                while (buffer.hasRemaining()) {
                    System.out.println((char) buffer.get());
                }
                System.out.println(" ");
                buffer.clear();
                i = channel.read(buffer);
            }
            channel.close();

            FileOutputStream outputStream = new FileOutputStream(filePath);
            FileChannel fileChannel = outputStream.getChannel();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
