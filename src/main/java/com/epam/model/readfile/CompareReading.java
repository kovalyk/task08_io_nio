package com.epam.model.readfile;

import java.io.*;

public class CompareReading {

    String path = "/home/traveler1357/Epam/Lesson12/src/main/resources/Entrepreneur.pdf";

    public void getUsualReader() throws IOException {
        final long start = System.nanoTime();
        int count = 0;
        System.out.println("Test usual reader");
        InputStream inputStream = new FileInputStream(path);
        int data = inputStream.read();
        while (data != -1) {
            data = inputStream.read();
            count++;
        }
        inputStream.close();
        final long end = System.nanoTime();
        System.out.println("count " + count);
        System.out.println("End test usual reader " + +(end - start) / 1000000000 + "s");
    }

    public void getBufferedReader() throws IOException {
        int count = 0;
        System.out.println("Test buffered reader");
        final long start = System.nanoTime();
        DataInputStream dataInputStream = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(path)));

        try {
            while (true) {
                byte b = dataInputStream.readByte();
                count++;
            }
        } catch (EOFException e) {
        }

        dataInputStream.close();
        System.out.println("count " + count);
        final long end = System.nanoTime();
        System.out.println("End test buffered reader " + (end - start) / 1000000000 + "s");

        int bufferedSize = 1 * 1024;
        System.out.println("Test with 1 MB buffer");
        final long start1 = System.nanoTime();
        DataInputStream dataInputStream2 = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(path), bufferedSize));
        count = 0;
        try {
            while (true) {
                byte b = dataInputStream2.readByte();
                count++;
            }
        } catch (EOFException e) {
        }
        dataInputStream.close();
        System.out.println("count " + count);
        final long end1 = System.nanoTime();
        System.out.println("End test with 1 MB buffer " + (end1 - start1) / 1000000000 + "s");
    }
}

