package com.epam.model.showcontent;

import java.io.File;
import java.io.IOException;

public class Content {

    public void printDirectory(File file, String str) {
        System.out.println(str + " Directory " + file.getName());
        str = str + " ";
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                printDirectory(f, str);
            } else {
                System.out.println(str + " Files " + f.getName());
            }
        }
    }

    public void getContent() throws IOException {
        System.out.println("Show content ");
        File file = new File("/home/traveler1357/Epam");
        if (file.exists()) {
            printDirectory(file, " ");
        } else {
            System.out.println("Directory does not exists");
        }
    }
}

